# PRESENTATION: CI IN REAL LIFE

This repository contains a Gitlab instance with a test project called `Brewery`.

The purpose is to demonstrate how Continuous Integration (CI) works in everyday
life with a simple project.

Why using a local gitlab instance?
Because during a presentation internet could be missing.

## Installation

```bash
$ sudo sh -c 'echo "0.0.0.0 gitlab.local.docker" >> /etc/hosts'
$ make install
$ sleep 60 # Wait few minutes please. Default Gitlab is long to install
$ make data-restore # Restore the data for the project
```

Wait few minutes then browse `http://gitlab.local.docker/`.

To login (default password is `azerty123`):

 * `root`: Administrator
 * `alice`: Maintainer
 * `bob`: Developer

You should have access to the [Brewery Repository](http://gitlab.local.docker/enseirb/brewery).
Clone it and feel free to work on it.

## Further reading

 * [Install GitLab with Docker images](https://docs.gitlab.com/omnibus/docker/)
 * [Run GitLab Runner in a container](https://docs.gitlab.com/runner/install/docker.html)
 * [Registering Runners](https://docs.gitlab.com/runner/register/index.html#docker)
