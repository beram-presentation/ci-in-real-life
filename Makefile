.PHONY: help

default: help

DATA_BACKUP_FOLDER?=data/backups
DATA_BACKUP_ID?=1542147235_2018_11_13_11.4.4
DATA_BACKUP_FILE?="$(DATA_BACKUP_ID)_gitlab_backup.tar"

RED    := \033[31m
GREEN  := \033[32m
WHITE  := \033[37m
YELLOW := \033[33m
RESET  := \033[0m

HELP_FUNC = \
    %help; \
    while(<>) { push @{$$help{$$2 // 'options'}}, [$$1, $$3] if /^([a-zA-Z\-]+)\s*:.*\#\#(?:@([a-zA-Z\-]+))?\s(.*)$$/ }; \
    print "usage: make [target]\n\n"; \
    for (sort keys %help) { \
    print "${WHITE}$$_:${RESET}\n"; \
    for (@{$$help{$$_}}) { \
    $$sep = " " x (32 - length $$_->[0]); \
    print "  ${YELLOW}$$_->[0]${RESET}$$sep${GREEN}$$_->[1]${RESET}\n"; \
    }; \
    print "\n"; }

help: ##@Miscellaneous Show this help.
	@perl -e '$(HELP_FUNC)' $(MAKEFILE_LIST)

###
### Project commands
###

.PHONY: install build clean cache-clear deploy

install: clean run ##@Project Install the project
	@echo "\n\n"
	@echo "$(YELLOW) >> Please wait few minutes for all container to start before doing something else...$(RESET)"
	@echo "$(YELLOW) >> Gitlab is quit long to install...$(RESET)"
	@echo "$(YELLOW) >> When you'll succeed to access http://gitlab.local.docker/ you should run 'make data-restore'...$(RESET)"
	@echo "\n\n"

run: ##@Project Run the project
	docker-compose kill
	docker-compose up --remove-orphans -d

clean: ##@Project Clean your repository
	rm -rvf srv/gitlab/data/*
	rm -rvf srv/gitlab/logs/*
	mkdir -p srv/gitlab/data
	mkdir -p srv/gitlab/logs

data-restore: ##@Project Restore to samples data
	@docker-compose exec gitlab.local.docker /bin/sh -c 'mkdir -p /var/opt/gitlab/backups'
	@docker cp $(DATA_BACKUP_FOLDER)/$(DATA_BACKUP_FILE) $(shell docker-compose ps -q gitlab.local.docker):/var/opt/gitlab/backups/$(DATA_BACKUP_FILE)
	docker-compose exec gitlab.local.docker gitlab-rake gitlab:backup:restore BACKUP=$(DATA_BACKUP_ID)

data-backup: ##@Project Backup the data
	docker-compose exec gitlab.local.docker gitlab-rake gitlab:backup:create
	echo "Don't forget to save your backup somewhere else than inside the container."
